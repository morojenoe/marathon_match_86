#include <cstdarg>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <sstream>
#include <functional>

#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <bitset>
#include <queue>
#include <deque>
#include <stack>

#include <cmath>
#include <string>
#include <cstring>
#include <string.h>

#include <memory.h>
#include <cassert>
#include <time.h>

using namespace std;

#define forn(i,n) for (int i = 0; i < (int)(n); i++)
#define fornd(i, n) for (int i = (int)(n) - 1; i >= 0; i--)
#define forab(i,a,b) for (int i = (int)(a); i <= (int)(b); i++)
#define forabd(i, b, a) for (int i = (int)(b); i >= (int)(a); i--)
#define forit(i, a) for (__typeof((a).begin()) i = (a).begin(); i != (a).end(); i++)

#define _(a, val) memset (a, val, sizeof (a))
#define sz(a) (int)((a).size())
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()

typedef long long lint;
typedef unsigned long long ull;
typedef long double ld;
typedef pair<int, int> pii;
typedef vector<int> vii;

const lint LINF = 1000000000000000000LL;
const int INF = 1000000000;
const long double eps = 1e-9;
const long double PI = 3.1415926535897932384626433832795l;

#ifdef MY_DEBUG
#define dbgx( x ) { cerr << #x << " = " << x << endl; }
#define dbg( ... ) { fprintf(stderr, __VA_ARGS__); fflush(stderr); }
#else
#define dbgx( x ) {  } 
#define dbg( ... ) {  } 
#endif

int sqr(int a)
{
	return a*a;
}

/*int abs(int a)
{
	if (a < 0)
		return -a;
	return a;
}

int max(int a, int b)
{
	if (a > b)
		return a;
	return b;
}*/

string pack(int a, int b, int c)
{
	char str[55];
	sprintf(str, "%d %d %d", a, b, c);
	return (string)str;
}

struct Edge
{
	int from, to, flow, cost, cap;
	Edge(){}
	Edge(int from, int to, int cap, int cost)
	{
		this->from = from;
		this->to = to;
		this->flow = 0;
		this->cost = cost;
		this->cap = cap;
	}
};

template<int MAX_VERTEXES>
class MinCostMaxFlow
{
public:
	void add_edge(int a, int b, int cap, int cost)
	{
		g[a].pb( sz(edges) );
		edges.pb( Edge(a, b, cap, cost) );

		g[b].pb( sz(edges) );
		edges.pb( Edge(b, a, 0, -cost) );
	}

	pii minCostMaxFlow()
	{
		int d[MAX_VERTEXES];
		int p[MAX_VERTEXES];

		while( 1 )
		{
			for(int i = 0; i < MAX_VERTEXES; i++)
			{
				d[i] = INF;
				p[i] = -1;
			}

			bool wasUpdate = true;
			while( wasUpdate )
			{
				wasUpdate = false;
				d[source] = 0;
				for(int i = 0; i < (int)edges.size(); i++)
				{
					Edge &e = edges[i];
					if (d[e.to] > d[e.from] + e.cost && e.cap - e.flow > 0)
					{
						d[e.to] = d[e.from] + e.cost;
						p[e.to] = i;
						wasUpdate = true;
					}
				}
			}

			if (d[target] == INF)
				break;

			int v = target;
			while(p[v] != -1)
			{
				int id = p[v];
				edges[id].flow ++;
				edges[id ^ 1].flow --;
				v = edges[id].from;
			}
		}

		int flow = 0, sum_cost = 0;
		for(int i = 0; i < (int)edges.size(); i++)
		{
			if (edges[i].flow > 0)
			{
				sum_cost += edges[i].flow*edges[i].cost;
				if (edges[i].from == source)
					flow += edges[i].flow;
			}
		}

		return mp(flow, sum_cost);
	}
	
	vector<int> g[MAX_VERTEXES];
	vector<Edge> edges;
	int source, target;
};

class Logging
{
public:
	Logging(const string &name)
	{
		f = fopen(name.c_str(), "w");
	}

	void print(const string &format, ...)
	{
		va_list argptr;
		va_start(argptr, format);
		vfprintf(f, format.c_str(), argptr);
		va_end(argptr);
		fflush(f);
	}

private:
	FILE* f;
};

Logging LOG("log.txt");

struct pos
{
	int x, y;
	pos(){}
	pos(int a, int b) { x = a; y = b; }
};

bool operator==(const pos &a, const pos &b)
{
	return a.x == b.x && a.y == b.y;
}

bool operator!=(const pos &a, const pos &b)
{
	return a.x != b.x || a.y != b.y;
}

bool operator<(const pos &a, const pos &b)
{
	return a.x < b.x || (a.x == b.x && a.y < b.y);
}

struct Answer
{
	int rawScore;
	vector<string> answer;
	Answer()
	{
		rawScore = 0;
		answer.clear();
	}
};

bool isValidMove(int dR, int dC)
{
	return dR == 0 || dC == 0 || dR == dC || dR == -dC;
}

int cntIntersections(const vector<pos> &positions, int i)
{
	int result = 0;
	int n = (int)positions.size();
	for(int j = 0; j < n; j++)
	{
		if (i != j && isValidMove(positions[i].x - positions[j].x, positions[i].y - positions[j].y))
		{
			result ++;
		}
	}
	return result;
}

int cntIntersections(const vector<pos> &positions)
{
	int result = 0;
	int n = (int)positions.size();
    for(int i = 0; i < n; i++)
	{
		for(int j = i + 1; j < n; j++)
		{
			if (isValidMove(positions[i].x - positions[j].x, positions[i].y - positions[j].y))
			{
				result ++;
			}
		}
    }
	return result;
}

bool checkPositions(const vector<pos> &positions)
{
	int n = (int)positions.size();
    for(int i = 0; i < n; i++)
	{
		for(int j = i + 1; j < n; j++)
		{
			if (isValidMove(positions[i].x - positions[j].x, positions[i].y - positions[j].y))
			{
				return false;
			}
		}
    }
	return true;
}

bool validMove(const vector<pos> &queens, int ind, const pos &curPos, const pos &newPos)
{
	if (curPos == newPos) return false;
	int dRow = newPos.x - curPos.x;
	int dCol = newPos.y - curPos.y;
	if (!isValidMove(dRow, dCol))
		return false;
	int dist = max(abs(dRow), abs(dCol));
	dRow /= dist;
	dCol /= dist;
	for (int j = 0; j < (int)queens.size(); ++j)
	{
        if (ind == j)
            continue;
        int dRowJ = queens[j].x - curPos.x;
        int dColJ = queens[j].y - curPos.y;
        if (!isValidMove(dRowJ, dColJ))
            continue;
        int distJ = max(abs(dRowJ), abs(dColJ));
        dRowJ /= distJ;
        dColJ /= distJ;
        if (dRow == dRowJ && dCol == dColJ && distJ <= dist)
            return false;
    }
	return true;
}

class PermutationForGA
{
public:
	PermutationForGA(int n)
	{
		p.resize(n);
		for(int i = 0; i < n; i++)
			p[i] = i;
		random_shuffle(p.begin(), p.end());
		calcFitness();
	}

	vector<pos> getPositions()
	{
		vector<pos> positions;
		for(int i = 0; i < (int)p.size(); i++)
			positions.push_back( pos(i, p[i]) );
		return positions;
	}

	void calcFitness()
	{
		fitness = cntIntersections(getPositions());
	}

	bool operator<(const PermutationForGA &oth) const
	{
		return fitness < oth.fitness;
	}

	void mutateTwoElements()
	{
		int x = rand() % (int)p.size();
		int y = rand() % (int)p.size();
		if (x == y)
			random_shuffle(p.begin(), p.end());
		swap(p[x], p[y]);
		calcFitness();
	}

	vector<int> p;
	int fitness;
};

class GAforPlaceFinder
{
public:
	GAforPlaceFinder(int sizeOfPopulation)
	{
		this->sizeOfPopulation = sizeOfPopulation;
	}

	vector<vector<pos> > getPlaces(int n)
	{
		vector<vector<pos> > result;
		vector<PermutationForGA> population(sizeOfPopulation, PermutationForGA(n));

		while((int)result.size() == 0)
		{
			for(int i = 0; i < (int)population.size(); i++)
				if (population[i].fitness == 0)
					result.push_back(population[i].getPositions());
			sort(population.begin(), population.end());
			dbgx(population[0].fitness);
			for(int i = 0; i < sizeOfPopulation / 2; i++)
			{
				population[sizeOfPopulation - 1 - i] = population[i];
				population[sizeOfPopulation - 1 - i].mutateTwoElements();
			}
		}

		return result;
	}

private:
	int sizeOfPopulation;
};

class PlaceFinder
{
public:
	vector<vector<pos> > getPlaces(int n)
	{
		GAforPlaceFinder geneticAlgorithm(40);
		return geneticAlgorithm.getPlaces(n);
	}

private:
	vector<vector<pos> > tryAllPermutations(int n)
	{
		vector<vector<pos> > result;
		vector<int> p;
		for(int i = 0; i < n; i++)
			p.push_back( i );
		do
		{
			vector<pos> cur;
			for(int i = 0; i < n; i++)
				cur.push_back( pos(i, p[i]) );
			if (checkPositions(cur))
				result.push_back( cur );
		} while(next_permutation(all(p)));
		return result;
	}
};

int manhattanDistance(const pos &a, const pos &b)
{
	return abs(a.x - b.x) + abs(a.y - b.y);
}

vector<int> assignPlacesToQueens(const vector<pos> &queens, const vector<pos> &places)
{
	MinCostMaxFlow<202> f;
	int source = 200;
	int target = 201;
	f.source = source;
	f.target = target;
	for(int i = 0; i < (int)queens.size(); i++)
	{
		f.add_edge(source, i, 1, 0);
	}
	for(int i = 0; i < (int)queens.size(); i++)
	{
		for(int j = 0; j < (int)places.size(); j++)
		{
			f.add_edge(i, j + (int)queens.size(), 1, manhattanDistance(queens[i], places[j]));
		}
	}
	for(int i = 0; i < (int)places.size(); i++)
	{
		f.add_edge((int)queens.size() + i, target, 1, 0);
	}
	
	vector<int> p((int)queens.size(), -1);
	pair<int, int> flow = f.minCostMaxFlow();
	assert(flow.first == (int)queens.size() );
	for(int i = 0; i < (int)f.edges.size(); i++)
	{
		Edge &e = f.edges[i];
		if (e.to != f.target && e.from != source && e.cap > 0 && e.flow > 0)
		{
			p[e.from] = e.to - (int)queens.size();
		}
	}

	return p;
}

int normalize(int a)
{
	if (a == 0) return a;
	if (a < 0) return -1;
	return 1;
}

pos movePosByVec(const pos &p, int dx, int dy)
{
	pos result;
	assert(isValidMove(dx, dy));
	result.x = p.x + dx;
	result.y = p.y + dy;
	return result;
}



struct StateBeamSearch
{
	static const int NMAX = 100;
	vector<pos> a;
	vector<string> answer;
	int cost;
};

class MemoryManager
{
public:
	deque<int> q;
	bool isEmpty()
	{
		return q.empty();
	}

	int get()
	{
		assert( !q.empty() );
		int res = q.front();
		q.pop_front();
		return res;
	}

	void push(int id)
	{
		q.push_front( id );
	}
};

const int MAX_WIDTH = 100;
const int MAX_STATES = 2*MAX_WIDTH;
StateBeamSearch state[MAX_STATES];
MemoryManager mm;

int computeCostForState(const StateBeamSearch &state, const vector<pos> &places)
{
	const int K1 = 100;
	int res = 0;

	for(int i = 0; i < (int)state.a.size(); i++)
	{
		if (state.a[i] == places[i])
			res -= K1;
		else
			res += sqr(manhattanDistance(state.a[i], places[i]));
	}

	return res;
}

bool cmpForBeamSearch(int i, int j)
{
	return state[i].cost < state[j].cost;
}

bool answerFound(int id, const vector<pos> &places)
{
	for(int i = 0; i < (int)state[id].a.size(); i++)
		if (places[i] != state[id].a[i])
			return false;
	return true;
}

Answer beamSearch(vector<pos> queens, const vector<pos> &places, int width)
{
	int MAGIC_CONSTANTA = 10;
	Answer result;
	result.rawScore = 2*INF;

	state[0].a = queens;
	state[0].cost = computeCostForState(state[0], places);

	int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
	int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};
	priority_queue<int, vector<int>, function<bool(int, int)> > pq1(cmpForBeamSearch), pq2(cmpForBeamSearch);
	pq1.push( 0 );
	mm.q.clear();
	for(int i = 1; i < 2*width; i++)
		mm.push( i );

	vector<int> ids(queens.size());
	for(int i = 0; i < (int)queens.size(); i++)
		ids[i] = i;

	int previousCost = -INF;
	int Time = 1;
	while( !answerFound(pq1.top(), places) )
	{
		Time ++;
		int cur = pq1.top();
		pq1.pop();
		StateBeamSearch &curState = state[cur];

		sort(ids.begin(), ids.end(), [&](int a, int b) -> bool {
			int d1, d2;
			d1 = manhattanDistance(curState.a[a], places[a]);
			d2 = manhattanDistance(curState.a[b], places[b]);
			if (d1 == 0)
				return false;
			return d1 > d2;
		});

		int pIdsr, pIdsl;
		pIdsr = (int)curState.a.size() - 1;
		while(pIdsr >= 0 && curState.a[pIdsr] == places[pIdsr]) 
			pIdsr --;

		for(pIdsl = 0; pIdsl < min(MAGIC_CONSTANTA, (int)curState.a.size()) && pIdsl <= pIdsr; pIdsl++, pIdsr --)
		{
			for(int _ = 0; _ < 2; _++)
			{
				int i = ids[pIdsl];
				if (curState.a[i] != places[i])
				{
					for(int j = 0; j < 8; j++)
					{
						for(int k = 1; k <= 10; k++)
						{
							pos curPos = curState.a[i];
							pos newPos = pos(curPos.x + dx[j]*k, curPos.y + dy[j]*k);
							if (manhattanDistance(curPos, places[i]) < manhattanDistance(newPos, places[i]))
								break;
							if (validMove(curState.a, i, curPos, newPos))
							{
								if (pq2.size() == width)
								{
									mm.push( pq2.top() );
									pq2.pop();
								}
								int id = mm.get();
								state[id] = curState;
								state[id].a[i] = newPos;
								state[id].answer.push_back(pack(i, newPos.x, newPos.y));
								state[id].cost = computeCostForState(state[id], places);
								pq2.push( id );
							}
							else
								break;
						}
					}
				}
				swap(pIdsl, pIdsr);
			}
		}

		mm.push(cur);
		if (pq1.empty())
		{
			dbg("%d %d %d\n", Time, curState.cost, previousCost);
			if (previousCost == curState.cost)
				MAGIC_CONSTANTA += 5;
			previousCost = curState.cost;
			swap(pq1, pq2);
		}
	}

	result.rawScore = state[pq1.top()].cost;
	result.answer = state[pq1.top()].answer;
	return result;
}

Answer makeMoves(vector<pos> queens, vector<pos> places, const vector<int> &p)
{
	vector<pos> places2(places.size());
	for(int i = 0; i < (int)p.size(); i++)
		places2[i] = places[p[i]];
	return beamSearch(queens, places2, 10);
}

pos getCenter(const vector<pos> &v)
{
	int minX, minY, maxX, maxY;
	minX = minY = INF;
	maxX = maxY = -INF;
	for(int i = 0; i < (int)v.size(); i++)
	{
		minX = min(minX, v[i].x);
		minY = min(minY, v[i].y);
		maxX = max(maxX, v[i].x);
		maxY = max(maxY, v[i].y);
	}

	return pos((minX + maxX) / 2, (minY + maxY) / 2);
}

void movePlaces(const vector<pos> &queens, vector<pos> &places)
{
	pos p, q;
	q = getCenter(queens);
	p = getCenter(places);
	p = pos(q.x - p.x, q.y - p.y);
	for(int i = 0; i < (int)places.size(); i++)
		places[i] = pos(places[i].x + p.x, places[i].y + p.y);
}

class MovingNQueens
{
public:
	vector<string> rearrange(vector<int> queenRows, vector<int> queenCols)
	{
		vector<pos> queens;
		for(int i = 0; i < (int)queenRows.size(); i++)
			queens.push_back( pos(queenRows[i], queenCols[i]) );
		vector<vector<pos> > places = PlaceFinder().getPlaces((int)queens.size());
		LOG.print("Places founded in %d ms\n", (int)clock());
		Answer best;
		best.rawScore = 2*INF;
		for(int i = 0; i < (int)places.size(); i++)
		{
			movePlaces(queens, places[i]);
			vector<int> p = assignPlacesToQueens(queens, places[i]);
			Answer cur = makeMoves(queens, places[i], p);
			if (best.rawScore > cur.rawScore)
				best = cur;
		}
		LOG.print("Answer found in %d ms\n", (int)clock());
		return best.answer;
	}
};

void gen(vector<int> &RQ, vector<int> &CQ)
{
	srand(0xAC);
	int a[105][105];
	_(a, -1);
	int n = 100;

	int minsz = ((int)sqrt(n + 0.)) + 1;
	int maxsz = 2*minsz;

	int sz = minsz + rand() % (maxsz - minsz + 1);
	sz = 10;
	forn(i, n)
	{
		int x = rand() % sz;
		int y = rand() % sz;
		while(a[x][y] != -1)
		{
			x = rand() % sz;
			y = rand() % sz;	
		}
		a[x][y] = 1;
		RQ.pb( x );
		CQ.pb( y );
	}
}

int main()
{
#ifdef MY_DEBUG
	freopen ("input.txt", "r", stdin);
#endif
	
	vector<int> queenRows, queenCols;
	/*int len;
	scanf("%d", &len);
	queenRows.resize(len);
	for(int i = 0; i < len; i++)
		scanf("%d", &queenRows[i]);
	scanf("%d", &len);
	queenCols.resize(len);
	for(int i = 0; i < len; i++)
		scanf("%d", &queenCols[i]);*/
	gen(queenRows, queenCols);
	vector<string> ret = MovingNQueens().rearrange(queenRows, queenCols);
	printf("%d\n", (int)ret.size());
	for(int i = 0; i < (int)ret.size(); i++)
		printf("%s\n", ret[i].c_str());
	fflush(stdout);

	LOG.print("Time %d\n", (int)clock());

	return 0;
}
